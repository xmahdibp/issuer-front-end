// @refresh reload
import "./root.css"
import { Routes } from "solid-start/root";
import { ErrorBoundary } from "solid-start/error-boundary";
import { Suspense } from "solid-js";
import {createTheme, ThemeProvider} from "@suid/material";
import {QueryProvider} from "~/contexts/QueryContext";
import {ValidatedQueryProvider} from "~/contexts/ValidatedQueryContext";
import {ExchangeProfileProvider} from "~/contexts/ExchangeProfileContext";
import {HopeProvider, NotificationsProvider} from "@hope-ui/solid";

const customTheme = createTheme({
  palette: {
    primary: {
      main: "#799aaf",
      contrastText: "white",
    },
  },
});

export default function Root() {
  return (
    <>
      <ErrorBoundary>
        <Suspense>
          <HopeProvider>
            <NotificationsProvider>
              <ThemeProvider theme={customTheme}>
                <QueryProvider>
                  <ValidatedQueryProvider>
                    <ExchangeProfileProvider>
                      <Routes />
                    </ExchangeProfileProvider>
                  </ValidatedQueryProvider>
                </QueryProvider>
              </ThemeProvider>
            </NotificationsProvider>
          </HopeProvider>
        </Suspense>
      </ErrorBoundary>
    </>
  );
}
