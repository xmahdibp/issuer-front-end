import {createSignal, createContext, useContext} from "solid-js";
import {ExchangeProfile} from "~/components/model/ExchangeProfile";

const ExchangeProfileContext = createContext();

export function ExchangeProfileProvider(props) {
  const [exchangeProfiles, setExchangeProfiles] = createSignal<ExchangeProfile[]>([]);
  const [exchangeProfilesInitialized, setExchangeProfilesInitialized] = createSignal(false);
  const store = [exchangeProfiles, {
    exchangeProfilesInitialized() {
      return exchangeProfilesInitialized();
    },
    setExchangeProfiles(exchangeProfiles: ExchangeProfile[]) {
      setExchangeProfiles([...exchangeProfiles])
      setExchangeProfilesInitialized(true);
    },
    addExchangeProfile(exchangeProfile: ExchangeProfile) {
      setExchangeProfiles([...exchangeProfiles(), exchangeProfile])
    },
    removeExchangeProfile(removedExchangeProfile: ExchangeProfile) {
      setExchangeProfiles([...exchangeProfiles().filter(exchangeProfile => exchangeProfile.id !== removedExchangeProfile.id)])
    },
    getExchangeProfile(id: string) {
      return exchangeProfiles()
          .find((profile) => profile.id === id);
    },
    async fetchExchangeProfiles() {
      const exchangeProfiles = await fetch(`${import.meta.env.VITE_BASE_URL_API}/v1/exchange-profile`);
      setExchangeProfiles(await exchangeProfiles.json())
      setExchangeProfilesInitialized(true);
    }
  }];

  return (
      <ExchangeProfileContext.Provider value={store}>
          {props.children}
      </ExchangeProfileContext.Provider>
  );
}

export function useExchangeProfiles() {
  return useContext(ExchangeProfileContext);
}
