import {createSignal, createContext, useContext} from "solid-js";
import {Query} from "~/components/model/Query";
import {ValidatedQuery} from "~/components/model/ValidatedQuery";

const ValidatedQueryContext = createContext();

export function ValidatedQueryProvider(props) {
  const [queries, setValidatedQueries] = createSignal<ValidatedQuery[]>([]);
  const [validatedQueriesInitialized, setValidatedQueriesInitialized] = createSignal(false);
  const store = [queries, {
    validatedQueriesInitialized() {
      return validatedQueriesInitialized();
    },
    setValidatedQueries(queries: ValidatedQuery[]) {
      setValidatedQueries([...queries])
      setValidatedQueriesInitialized(true);
    },
    addValidatedQuery(validatedQuery: ValidatedQuery) {
      setValidatedQueries([...queries(), validatedQuery])
    },
    removeValidatedQuery(removedValidatedQuery: ValidatedQuery) {
      setValidatedQueries([...queries().filter(validatedQuery => validatedQuery.id !== removedValidatedQuery.id)])
    },
    async fetchValidatedQueries() {
      const validatedQueries = await fetch(`${import.meta.env.VITE_BASE_URL_API}/v1/validated-query`);
      setValidatedQueries(await validatedQueries.json())
      setValidatedQueriesInitialized(true);
    }
  }];

  return (
      <ValidatedQueryContext.Provider value={store}>
          {props.children}
      </ValidatedQueryContext.Provider>
  );
}

export function useValidatedQueries() {
  return useContext(ValidatedQueryContext);
}
