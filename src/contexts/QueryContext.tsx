import {createSignal, createContext, useContext} from "solid-js";
import {Query} from "~/components/model/Query";

const QueryContext = createContext();

export function QueryProvider(props) {
  const [queries, setQueries] = createSignal<Query[]>([]);
  const [queriesInitialized, setQueriesInitialized] = createSignal(false);
  const store = [queries, {
    queriesInitialized() {
      return queriesInitialized()
    },
    setQueries(queries: Query[]) {
      setQueries([...queries])
    },
    addQuery(query: Query) {
      setQueries([...queries(), query])
    },
    removeQuery(removedQuery: Query) {
      setQueries([...queries().filter(query => query.id !== removedQuery.id)])
    },
    async fetchQueries() {
      const queriesResponse = await fetch(`${import.meta.env.VITE_BASE_URL_API}/v1/query`);
      setQueries(await queriesResponse.json())
    }
  }];

  return (
      <QueryContext.Provider value={store}>
          {props.children}
      </QueryContext.Provider>
  );
}

export function useQueries() {
  return useContext(QueryContext);
}
