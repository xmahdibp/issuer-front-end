import CssBaseline from "@suid/material/CssBaseline";
import AppBar from "~/components/AppBar";
import SideBar from "~/components/sidebar/SideBar";
import {useValidatedQueries} from "~/contexts/ValidatedQueryContext";
import {For, onMount, Suspense} from "solid-js";
import Grid from "@suid/material/Grid";
import CircularProgress from "@suid/material/CircularProgress";
import CreateValidatedQueryModal from "~/components/modal/CreateValidatedQueryModal";
import Alert from "@suid/material/Alert";
import {ValidatedQuery} from "~/components/model/ValidatedQuery";
import ValidatedQueryCard from "~/components/card/ValidatedQueryCard";

export default () => {

  // @ts-ignore
  const [validatedQueries, {validatedQueriesInitialized, fetchValidatedQueries}] = useValidatedQueries();

  onMount(async () => {
    if(!validatedQueriesInitialized()) {
      fetchValidatedQueries();
    }
  })

  return (
      <>
        <CssBaseline/>
        <AppBar/>
        <SideBar/>
        <main>
          <Grid
              container
              spacing={2}
              direction="row"
              justifyContent="flex-start"
              alignItems="flex-start"
          >
            <Suspense fallback={<div class="container"><CircularProgress/></div>}>
              <For
                  fallback={(
                      <Grid item xs={12}>
                        <Alert severity="info">Geen gevalideerde vragen gevonden.</Alert>
                      </Grid>
                  )}
                  each={validatedQueries()}
              >{(validatedQuery: ValidatedQuery) =>
                  <Grid item xs={12} md={6} lg={6} xl={6}>
                    <ValidatedQueryCard validatedQuery={validatedQuery} />
                  </Grid>
              }</For>
            </Suspense>
          </Grid>
          <CreateValidatedQueryModal/>
        </main>
      </>
  );
}
