import CssBaseline from "@suid/material/CssBaseline";
import AppBar from "~/components/AppBar";
import SideBar from "~/components/sidebar/SideBar";
import Grid from "@suid/material/Grid";
import {For, onMount, Suspense} from "solid-js";
import CreateQueryModal from "~/components/modal/CreateQueryModal";
import CircularProgress from "@suid/material/CircularProgress";
import {Query} from "~/components/model/Query";
import QueryCard from "~/components/card/QueryCard";
import {useQueries} from "~/contexts/QueryContext";
import Alert from "@suid/material/Alert";

export default () => {

  // @ts-ignore
  const [queries, {queriesInitialized, fetchQueries}] = useQueries();

  onMount(async() => {
    if(!queriesInitialized()) {
      fetchQueries();
    }
  })

  return (
      <>
        <CssBaseline/>
        <AppBar/>
        <SideBar/>
        <main>
          <Grid
              container
              spacing={2}
              direction="row"
              justifyContent="flex-start"
              alignItems="flex-start"
          >
            <Suspense fallback={<div class="container"><CircularProgress/></div>}>
              <For
                  fallback={(
                      <Grid item xs={12}>
                        <Alert severity="info">Geen queries gevonden.</Alert>
                      </Grid>
                  )}
                  each={queries()}
              >{(query: Query) =>
                  <Grid item xs={12} md={6} lg={6} xl={6}>
                    <QueryCard query={query} />
                  </Grid>
              }</For>
            </Suspense>
          </Grid>
          <CreateQueryModal/>
        </main>
      </>
  );
}
