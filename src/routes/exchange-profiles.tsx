import CssBaseline from "@suid/material/CssBaseline";
import AppBar from "~/components/AppBar";
import SideBar from "~/components/sidebar/SideBar";
import TableCell from "@suid/material/TableCell";
import TableRow from "@suid/material/TableRow";
import TableBody from "@suid/material/TableBody";
import TableHead from "@suid/material/TableHead";
import TableContainer from "@suid/material/TableContainer";
import Table from "@suid/material/Table";
import {For, onMount, Suspense} from "solid-js";
import {useExchangeProfiles} from "~/contexts/ExchangeProfileContext";
import CircularProgress from "@suid/material/CircularProgress";
import CreateExchangeProfileModal from "~/components/modal/CreateExchangeProfileModal";
import {ExchangeProfile} from "~/components/model/ExchangeProfile";
import DeleteIcon from "@suid/icons-material/Delete";
import IconButton from "@suid/material/IconButton";
import PrimaryCard from "~/components/card/PrimaryCard";
import {notificationService} from "@hope-ui/solid";

const deleteExchangeProfileOnServer = async (exchangeProfile: ExchangeProfile) => {
  if(window.confirm(`Weet je zeker dat je het uitwisselprofiel "${exchangeProfile.name}" wilt verwijderen?`)) {
      const response = await fetch(`${import.meta.env.VITE_BASE_URL_API}/v1/exchange-profile`, {
        method: "delete",
        body: JSON.stringify(exchangeProfile),
        headers: {
          'Content-Type': 'application/json'
        },
      });

      if(!response.ok) {
        const jsonResponse = await response.json();
        notificationService.show({
          status: "danger",
          title: "Er is iets fout gegaan",
          description: "Het verwijderen is helaas mislukt. De server meldt het volgende: " + jsonResponse.message
        });
      }

      return response.ok;
  }
  return false;
}

export default () => {

  // @ts-ignore
  const [exchangeProfiles, {exchangeProfilesInitialized, removeExchangeProfile, fetchExchangeProfiles}] = useExchangeProfiles();

  onMount(async() => {
    if(!exchangeProfilesInitialized()) {
      fetchExchangeProfiles();
    }
  })

  return (
      <>
        <CssBaseline/>
        <AppBar/>
        <SideBar/>
        <main>
            <Suspense fallback={<div class="container"><CircularProgress/></div>}>
              <TableContainer component={PrimaryCard}>
                <Table sx={{minWidth: 650}} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Naam</TableCell>
                      <TableCell>Beschrijving</TableCell>
                      <TableCell>URL</TableCell>
                      <TableCell>Delete</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <For
                        fallback={
                          <TableRow
                              sx={{"&:last-child td, &:last-child th": {border: 0}}}
                          >
                            <TableCell component="th" scope="row">
                              Geen uitwissenprofielen gevonden
                            </TableCell>
                          </TableRow>
                    }
                        each={exchangeProfiles()}
                    >{(exchangeProfile: ExchangeProfile) =>
                        <TableRow
                            sx={{"&:last-child td, &:last-child th": {border: 0}}}
                        >
                          <TableCell component="th" scope="row">
                            {exchangeProfile.name}
                          </TableCell>
                          <TableCell>{exchangeProfile.description}</TableCell>
                          <TableCell>
                            <a href={exchangeProfile.url} target="_blank">{exchangeProfile.url}</a>
                          </TableCell>
                          <TableCell>
                            <IconButton
                                color="error"
                                onClick={async () => {
                                  if(await deleteExchangeProfileOnServer(exchangeProfile)) {
                                    removeExchangeProfile(exchangeProfile); //remove from the store
                                  }
                                }}
                            >
                              <DeleteIcon/>
                            </IconButton>
                          </TableCell>
                        </TableRow>
                    }</For>
                  </TableBody>
                </Table>
              </TableContainer>
            </Suspense>
          <CreateExchangeProfileModal/>
        </main>
      </>
  );
}
