import AppBar from "@suid/material/AppBar";
import Box from "@suid/material/Box";
import Toolbar from "@suid/material/Toolbar";
import Typography from "@suid/material/Typography";
import Avatar from "@suid/material/Avatar";
import Paper from "@suid/material/Paper";
import IconButton from "@suid/material/IconButton";
import InputBase from "@suid/material/InputBase";
import MenuIcon from '@suid/icons-material/Menu';
import SearchIcon from '@suid/icons-material/Search';
import Grid from "@suid/material/Grid";


export default () => {
  return (
      <Box>
        <AppBar position="fixed" sx={{ backgroundColor: "rgba(0, 0, 0, 0.5)"}}>
          <Toolbar>
            <Typography
                variant="h6"
                noWrap
                component="a"
                href="/"
                sx={{
                  mr: 2,
                  overflow: "visible",
                  fontFamily: 'monospace',
                  fontWeight: 700,
                  letterSpacing: '.3rem',
                  color: 'inherit',
                  textDecoration: 'none',
                }}
            >
              KIK-V Issuer
            </Typography>

            <Grid
                container
                direction="row"
                justifyContent="flex-end"
                alignItems="center"
            >
              <Paper
                  component="form"
                  sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', backgroundColor: "rgba(0, 0, 0, 0.3)" }}
              >
                <IconButton sx={{ p: '10px', color: '#e9eeee' }} aria-label="menu">
                  <MenuIcon />
                </IconButton>
                <InputBase
                    sx={{ ml: 1, flex: 1, color: '#e9eeee' }}
                    placeholder="Search"
                    inputProps={{ 'aria-label': 'search' }}
                />
                <IconButton type="submit" sx={{ p: '10px', color: '#e9eeee' }} aria-label="search">
                  <SearchIcon />
                </IconButton>
              </Paper>
              <Avatar sx={{ml: 2}} alt="Remy Sharp" src="https://mui.com/static/images/avatar/2.jpg" />
            </Grid>
          </Toolbar>
        </AppBar>
      </Box>
  );
}
