import PrimaryCard from "~/components/card/PrimaryCard";
import {batch, createEffect, createResource, createSignal, For, onMount, Show} from "solid-js";
import Button from "@suid/material/Button";
import AddIcon from "@suid/icons-material/Add";
import SaveIcon from '@suid/icons-material/Save';
import Fab from "@suid/material/Fab";
import CardHeader from "@suid/material/CardHeader";
import CardContent from "@suid/material/CardContent";
import CardActions from "@suid/material/CardActions";
import TextField from "@suid/material/TextField";
import Grid from "@suid/material/Grid";
import {Query} from "~/components/model/Query";
import Drawer from "@suid/material/Drawer";
import {useQueries} from "~/contexts/QueryContext";
import {Organization} from "~/components/model/Organization";
import {useValidatedQueries} from "~/contexts/ValidatedQueryContext";
import {ValidatedQuery} from "~/components/model/ValidatedQuery";
import {
  notificationService,
  Select,
  SelectContent,
  SelectIcon,
  SelectListbox,
  SelectOption, SelectOptionIndicator, SelectOptionText,
  SelectPlaceholder,
  SelectTrigger,
  SelectValue
} from "@hope-ui/solid";

const fetchOrganizations = async (): Promise<Organization[]> => {
  const resp = await fetch(`${import.meta.env.VITE_BASE_URL_API}/v1/nuts/organization`)

  if(resp.ok) {
    const json = await resp.json();

    return json.map(nutsOrg => {
      return {
        controller: nutsOrg.didDocument.controller,
        name: nutsOrg.organization.name,
        city: nutsOrg.organization.city,
        did: nutsOrg.didDocument.id
      }
    })
  }
}

export default () => {

  const [open, setOpen] = createSignal(false);
  const [selectedQueries, setSelectedQueries] = createSignal<Query[]>([]);
  const [name, setName] = createSignal("");
  const [issuerDid, setIssuerDid] = createSignal("");
  const [issuerName, setIssuerName] = createSignal("");
  const [issuerCity, setIssuerCity] = createSignal("");
  const [subjectDid, setSubjectDid] = createSignal("");
  const [subjectName, setSubjectName] = createSignal("");
  const [subjectCity, setSubjectCity] = createSignal("");

  const [organizations] = createResource(fetchOrganizations);
  const [ownOrganizations, setOwnOrganizations] = createSignal<Organization[]>();

  createEffect(() => {
    if(organizations()) {
      setOwnOrganizations(
          organizations()
          //TODO: Commenting out due to often changing controller did, enable when stable
              // .filter(org => org.controller === import.meta.env.VITE_SERVICE_PROVIDER_CONTROLLER_DID)
      )
    }
  })

  // @ts-ignore
  const [validatedQueries, {addValidatedQuery}] = useValidatedQueries();

  // @ts-ignore
  const [queries, {queriesInitialized, fetchQueries}] = useQueries();

  onMount(async() => {
    if(!queriesInitialized()) {
      fetchQueries();
    }
  })

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const clearValues = () => {
    batch(() => {
      setName("");
      setIssuerDid("");
      setIssuerName("");
      setIssuerCity("");
      setSubjectDid("");
      setSubjectName("");
      setSubjectCity("");
      setSelectedQueries([]);
    })
  }

  const createValidatedQuery = async (validatedQuery: ValidatedQuery) => {

    try {
      const createValidatedQueryResponse = await fetch(`${import.meta.env.VITE_BASE_URL_API}/v1/validated-query`, {
        method: "post",
        body: JSON.stringify(validatedQuery),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      });

      if(!createValidatedQueryResponse.ok) {
        const jsonResponse = await createValidatedQueryResponse.json();
        notificationService.show({
          status: "danger",
          title: "Er is iets fout gegaan",
          description: "Het aanmaken is helaas mislukt. De server meldt het volgende: " + jsonResponse.message
        });
      } else {
        handleClose();
        clearValues();
        addValidatedQuery(await createValidatedQueryResponse.json());
      }
    } catch(e) {
      notificationService.show({
        status: "danger",
        title: "Er is iets fout gegaan",
        description: `Het aanmaken is helaas mislukt. Dit is de foutmelding: ${typeof e === "string" ? e : e.message}`
      });
    }
  }

  return (
      <>
        <Fab
            variant="extended"
            sx={{
              position: "fixed",
              bottom: 16,
              right: 16,
              zIndex: 10
            }}
            color="primary"
            aria-label="add"
            onClick={handleOpen}
        >
          <AddIcon/>
          Aanmaken
        </Fab>
        <Drawer
            anchor="bottom"
            open={open()}
            onClose={handleClose}
            sx={{
              '& .MuiDrawer-paper': {
                borderRadius: "4px"
              }
            }}
        >
          <PrimaryCard sx={{mb: 0}}>
            <CardHeader title="Gevalideerde vraag aanmaken"></CardHeader>
            <CardContent>
              <Grid
                  container
                  spacing={2}
              >
                <Grid item xs={12}>
                  <TextField
                      fullWidth
                      required
                      value={name()}
                      label="Naam"
                      onChange={(e) => setName(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Select
                      value={selectedQueries().map(query => query.id)}
                      multiple
                      size="lg"
                      onChange={selectedQueryIds => {
                        setSelectedQueries(
                            queries().filter(query => selectedQueryIds.includes(query.id))
                        )
                      }}
                  >
                    <SelectTrigger>
                      <SelectPlaceholder>Kies queries die gebruikt mogen worden</SelectPlaceholder>
                      <SelectValue />
                      <SelectIcon />
                    </SelectTrigger>
                    <SelectContent>
                      <SelectListbox>
                        <For
                            each={queries()}
                            fallback={<option>Geen technische vraagstellingen gevonden</option>}
                        >{(query: Query) =>
                            <SelectOption value={query.id}>
                              <SelectOptionText>{query.name}</SelectOptionText>
                              <SelectOptionIndicator />
                            </SelectOption>
                        }</For>
                      </SelectListbox>
                    </SelectContent>
                  </Select>
                </Grid>
                <Grid item xs={12}>
                  <Select
                      value={issuerDid()}
                      size="lg"
                      onChange={orgDid => {
                        const selectedOrganization = organizations().find(org => org.did === orgDid);
                        setIssuerName(selectedOrganization.name);
                        setIssuerCity(selectedOrganization.city);
                        setIssuerDid(orgDid);
                      }}
                  >
                    <SelectTrigger>
                      <SelectPlaceholder>Kies de issuer</SelectPlaceholder>
                      <SelectValue />
                      <SelectIcon />
                    </SelectTrigger>
                    <SelectContent>
                      <SelectListbox>
                        <For
                            each={ownOrganizations()}
                            fallback={<option>Geen organizaties gevonden in NUTS</option>}
                        >{(organization: Organization) =>
                            <SelectOption value={organization.did}>
                              <SelectOptionText>{organization.name} ({organization.city})</SelectOptionText>
                              <SelectOptionIndicator />
                            </SelectOption>
                        }</For>
                      </SelectListbox>
                    </SelectContent>
                  </Select>
                </Grid>
                <Grid item xs={12}>
                  <Select
                      value={subjectDid()}
                      size="lg"
                      onChange={orgDid => {
                        const selectedOrganization = organizations().find(org => org.did === orgDid);
                        setSubjectName(selectedOrganization.name);
                        setSubjectCity(selectedOrganization.city);
                        setSubjectDid(orgDid);
                      }}
                  >
                    <SelectTrigger>
                      <SelectPlaceholder>Kies een subject</SelectPlaceholder>
                      <SelectValue />
                      <SelectIcon />
                    </SelectTrigger>
                    <SelectContent>
                      <SelectListbox>
                        <For
                            each={organizations()}
                            fallback={<option>Geen organizaties gevonden in NUTS</option>}
                        >{(organization: Organization) =>
                            <SelectOption value={organization.did}>
                              <SelectOptionText>{organization.name} ({organization.city})</SelectOptionText>
                              <SelectOptionIndicator />
                            </SelectOption>
                        }</For>
                      </SelectListbox>
                    </SelectContent>
                  </Select>
                </Grid>
              </Grid>
            </CardContent>
            <CardActions>
              <Button
                  variant="contained"
                  startIcon={<SaveIcon/>}
                  onClick={() => createValidatedQuery({
                    name: name(),
                    queries: selectedQueries(),
                    issuerDid: issuerDid(),
                    issuerName: issuerName(),
                    issuerCity: issuerCity(),
                    subjectDid: subjectDid(),
                    subjectName: subjectName(),
                    subjectCity: subjectCity()
                  })}
              >
                Opslaan
              </Button>
            </CardActions>
          </PrimaryCard>
        </Drawer>
      </>
  )
}
