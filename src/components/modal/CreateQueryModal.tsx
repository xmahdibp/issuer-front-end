import PrimaryCard from "~/components/card/PrimaryCard";
import {batch, createSignal, For, onMount, Show} from "solid-js";
import Button from "@suid/material/Button";
import AddIcon from "@suid/icons-material/Add";
import SaveIcon from '@suid/icons-material/Save';
import Fab from "@suid/material/Fab";
import CardHeader from "@suid/material/CardHeader";
import CardContent from "@suid/material/CardContent";
import CardActions from "@suid/material/CardActions";
import TextField from "@suid/material/TextField";
import Grid from "@suid/material/Grid";
import {Query} from "~/components/model/Query";
import Drawer from "@suid/material/Drawer";
import {useQueries} from "~/contexts/QueryContext";
import {useExchangeProfiles} from "~/contexts/ExchangeProfileContext";
import {ExchangeProfile} from "~/components/model/ExchangeProfile";
import {
  notificationService,
  Select,
  SelectContent,
  SelectIcon,
  SelectListbox, SelectOption, SelectOptionIndicator, SelectOptionText,
  SelectPlaceholder,
  SelectTrigger,
  SelectValue, Textarea
} from "@hope-ui/solid";

export default () => {

  const [open, setOpen] = createSignal(false);
  const [queryName, setQueryName] = createSignal("");
  const [queryDescription, setQueryDescription] = createSignal("");
  const [queryOntology, setQueryOntology] = createSignal("");
  const [queryProfile, setQueryProfile] = createSignal<ExchangeProfile>();
  const [query, setQuery] = createSignal("");

  // @ts-ignore
  const [queries, {addQuery}] = useQueries();

  // @ts-ignore
  const [exchangeProfiles, {fetchExchangeProfiles, exchangeProfilesInitialized, getExchangeProfile}] = useExchangeProfiles();

  onMount(() => {
    if(!exchangeProfilesInitialized()) {
      fetchExchangeProfiles();
    }
  })

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const clearValues = () => {
    batch(() => {
      setQueryName("");
      setQueryDescription("");
      setQuery("");
      setQueryProfile(undefined);
    })
  }

  const createQuery = async (query: Query) => {

    try {
      const createQueryResponse = await fetch(`${import.meta.env.VITE_BASE_URL_API}/v1/query`, {
        method: "post",
        body: JSON.stringify(query),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      });

      if(!createQueryResponse.ok) {
        const jsonResponse = await createQueryResponse.json();
        notificationService.show({
          status: "danger",
          title: "Er is iets fout gegaan",
          description: "Het aanmaken is helaas mislukt. De server meldt het volgende: " + jsonResponse.message
        });
      } else {
        handleClose();
        clearValues();
        addQuery(await createQueryResponse.json());
      }
    } catch(e) {
      notificationService.show({
        status: "danger",
        title: "Er is iets fout gegaan",
        description: `Het aanmaken is helaas mislukt. Dit is de foutmelding: ${typeof e === "string" ? e : e.message}`
      });
    }
  }

  return (
      <>
        <Fab
            variant="extended"
            sx={{
              position: "fixed",
              bottom: 16,
              right: 16,
              zIndex: 10
            }}
            color="primary"
            aria-label="add"
            onClick={handleOpen}
        >
          <AddIcon/>
          Aanmaken
        </Fab>
        <Drawer
            anchor="bottom"
            open={open()}
            onClose={handleClose}
            sx={{
              '& .MuiDrawer-paper': {
                borderRadius: "4px"
              }
            }}
        >
          <PrimaryCard sx={{mb: 0}}>
            <CardHeader title="Technische vraagstelling aanmaken"></CardHeader>
            <CardContent>
              <Grid
                  container
                  spacing={2}
              >
                <Grid item xs={12}>
                  <Select size="lg" onChange={profileId => setQueryProfile(getExchangeProfile(profileId))}>
                    <SelectTrigger>
                      <SelectPlaceholder>Kies een uitwisselprofiel</SelectPlaceholder>
                      <SelectValue />
                      <SelectIcon />
                    </SelectTrigger>
                    <SelectContent>
                      <SelectListbox>
                        <For
                            each={exchangeProfiles()}
                            fallback={<option>Geen uitwisselprofielen gevonden</option>}
                        >{(exchangeProfile: ExchangeProfile) =>
                            <SelectOption value={exchangeProfile.id}>
                              <SelectOptionText>{exchangeProfile.name}</SelectOptionText>
                              <SelectOptionIndicator />
                            </SelectOption>
                        }</For>
                      </SelectListbox>
                    </SelectContent>
                  </Select>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                      fullWidth
                      required
                      value={queryName()}
                      label="Query Naam"
                      onChange={(e) => setQueryName(e.currentTarget.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                      fullWidth
                      required
                      value={queryDescription()}
                      label="Query Beschrijving"
                      onChange={(e) => setQueryDescription(e.currentTarget.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                      fullWidth
                      required
                      value={queryOntology()}
                      label="Ontologie"
                      onChange={(e) => setQueryOntology(e.currentTarget.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Textarea
                      value={query()}
                      placeholder="SPARQL Query"
                      size="lg"
                      onInput={(e) => setQuery(e.currentTarget.value)}
                  >

                  </Textarea>
                </Grid>
              </Grid>
            </CardContent>
            <CardActions>
              <Button
                  variant="contained"
                  startIcon={<SaveIcon/>}
                  onClick={() => createQuery({
                    name: queryName(),
                    description: queryDescription(),
                    query: query(),
                    ontology: queryOntology(),
                    profile: queryProfile()
                  })}
              >
                Opslaan
              </Button>
            </CardActions>
          </PrimaryCard>
        </Drawer>
      </>
  )
}
