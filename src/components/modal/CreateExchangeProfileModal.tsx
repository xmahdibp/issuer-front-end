import PrimaryCard from "~/components/card/PrimaryCard";
import {batch, createRenderEffect, createSignal, Show} from "solid-js";
import Button from "@suid/material/Button";
import AddIcon from "@suid/icons-material/Add";
import SaveIcon from '@suid/icons-material/Save';
import Fab from "@suid/material/Fab";
import CardHeader from "@suid/material/CardHeader";
import CardContent from "@suid/material/CardContent";
import CardActions from "@suid/material/CardActions";
import TextField from "@suid/material/TextField";
import Grid from "@suid/material/Grid";
import {ExchangeProfile} from "~/components/model/ExchangeProfile";
import Drawer from "@suid/material/Drawer";
import {useExchangeProfiles} from "~/contexts/ExchangeProfileContext";
import {notificationService} from "@hope-ui/solid";

export default () => {

  const [open, setOpen] = createSignal(false);
  const [exchangeProfileName, setExchangeProfileName] = createSignal("");
  const [exchangeProfileDescription, setExchangeProfileDescription] = createSignal("");
  const [exchangeProfileUrl, setExchangeProfileUrl] = createSignal("");

  // @ts-ignore
  const [exchangeProfiles, {addExchangeProfile}] = useExchangeProfiles();

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const clearValues = () => {
    batch(() => {
      setExchangeProfileName("");
      setExchangeProfileDescription("");
      setExchangeProfileUrl("");
    })
  }

  const createExchangeProfile = async (exchangeProfile: ExchangeProfile) => {

    try {
      const createExchangeProfileResponse = await fetch(`${import.meta.env.VITE_BASE_URL_API}/v1/exchange-profile`, {
        method: "post",
        body: JSON.stringify(exchangeProfile),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      });

      if(!createExchangeProfileResponse.ok) {
        const jsonResponse = await createExchangeProfileResponse.json();
        notificationService.show({
          status: "danger",
          title: "Er is iets fout gegaan",
          description: "Het aanmaken is helaas mislukt. De server meldt het volgende: " + jsonResponse.message
        });
      } else {
        handleClose();
        clearValues();
        addExchangeProfile(await createExchangeProfileResponse.json());
      }
    } catch(e) {
      notificationService.show({
        status: "danger",
        title: "Er is iets fout gegaan",
        description: `Het aanmaken is helaas mislukt. Dit is de foutmelding: ${typeof e === "string" ? e : e.message}`
      });
    }
  }

  return (
      <>
        <Fab
            variant="extended"
            sx={{
              position: "fixed",
              bottom: 16,
              right: 16,
              zIndex: 10
            }}
            color="primary"
            aria-label="add"
            onClick={handleOpen}
        >
          <AddIcon/>
          Aanmaken
        </Fab>
        <Drawer
            anchor="bottom"
            open={open()}
            onClose={handleClose}
            sx={{
              '& .MuiDrawer-paper': {
                borderRadius: "4px"
              }
            }}
        >
          <PrimaryCard sx={{mb: 0}}>
            <CardHeader title="Uitwisselprofiel aanmaken"></CardHeader>
            <CardContent>
              <Grid
                  container
                  spacing={2}
              >
                <Grid item xs={12}>
                  <TextField
                      fullWidth
                      required
                      value={exchangeProfileName()}
                      label="Naam"
                      onChange={(e) => setExchangeProfileName(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                      fullWidth
                      required
                      value={exchangeProfileDescription()}
                      label="Beschrijving"
                      onChange={(e) => setExchangeProfileDescription(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                      multiline
                      rows={4}
                      fullWidth
                      required
                      value={exchangeProfileUrl()}
                      label="URL"
                      onChange={(e) => setExchangeProfileUrl(e.target.value)}
                  />
                </Grid>
              </Grid>
            </CardContent>
            <CardActions>
              <Button
                  variant="contained"
                  startIcon={<SaveIcon/>}
                  onClick={() => createExchangeProfile({
                    name: exchangeProfileName(),
                    description: exchangeProfileDescription(),
                    url: exchangeProfileUrl(),
                  })}
              >
                Opslaan
              </Button>
            </CardActions>
          </PrimaryCard>
        </Drawer>
      </>
  )
}
