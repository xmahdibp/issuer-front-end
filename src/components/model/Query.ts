import {ExchangeProfile} from "~/components/model/ExchangeProfile";

export interface Query {
  name: string;
  description: string;
  query: string;
  ontology: string;
  profile: ExchangeProfile;
  id?: string;
  createdOn?: Date;
}
