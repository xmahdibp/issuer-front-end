export interface ExchangeProfile {
  name: string;
  description: string;
  url: string;
  id?: string;
  createdOn?: Date;
}
