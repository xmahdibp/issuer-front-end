import {Organization} from "~/components/model/Organization";
import {Query} from "~/components/model/Query";
import {VerifiableCredential} from "~/components/model/VerifiableCredential";

export interface ValidatedQuery {
  name: string;
  queries: Query[];
  issuerDid: string;
  issuerName: string;
  issuerCity: string;
  subjectDid: string;
  subjectName: string;
  subjectCity: string;
  id?: string;
  createdOn?: Date;
  verifiableCredentials?: string[]; //filled on create by NUTS
}
