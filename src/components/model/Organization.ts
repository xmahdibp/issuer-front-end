export interface Organization {
  controller: string;
  name: string;
  city: string;
  did: string;
}
