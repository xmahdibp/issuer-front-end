import ListItemIcon from "@suid/material/ListItemIcon";
import ListItemText from "@suid/material/ListItemText";
import ListItemButton from "@suid/material/ListItemButton";
import ListItem from "@suid/material/ListItem";
import {createMemo, createSignal, JSX} from "solid-js";
import {Link, useLocation} from "solid-app-router";
import "./SideBarItem.css"

interface Props {
  title: string;
  route: string;
  icon: JSX.Element;
}

export default ({title, route, icon}: Props) => {

  const location = useLocation();
  const pathname = createMemo(() => location.pathname);
  console.log(pathname())

  return (
      <Link class="nav" href={route}>
        <ListItem disablePadding>
          <ListItemButton selected={pathname() === route}>
            <ListItemIcon>
              {icon}
            </ListItemIcon>
            <ListItemText primary={title}/>
          </ListItemButton>
        </ListItem>
      </Link>
  )
}
