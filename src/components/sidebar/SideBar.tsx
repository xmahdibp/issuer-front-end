import Drawer from "@suid/material/Drawer";
import List from "@suid/material/List";
import {Box} from "@suid/material/Box";
import HomeIcon from "@suid/icons-material/Home";
import DataObjectIcon from '@suid/icons-material/DataObject';
import QuestionAnswerIcon from '@suid/icons-material/QuestionAnswer';
import AssignmentIcon from '@suid/icons-material/Assignment';
import SideBarItem from "~/components/sidebar/SideBarItem";

export default () => {
  const drawerWidth = 250;
  return (
      <Drawer
          variant="permanent"
          anchor="left"
          open={true}

          sx={{
            flexShrink: 0,
            BackdropProps: {
              background: 'transparent'
            },
            '& .MuiDrawer-paper': {
              height: "calc(100% - 64px)",
              mt: "64px",
              width: drawerWidth,
              boxSizing: 'border-box',
              backgroundColor: "rgba(0, 0, 0, 0.5)"
            }}}
      >
        <Box
            sx={{width: drawerWidth, color: "#e9eeee", height: "100vh"}}
            role="presentation"
        >
          <List sx={{padding: 0, mt: "20px"}}>
            <SideBarItem route="/exchange-profiles" title={"Uitwissenprofielen"} icon={<AssignmentIcon sx={{color: "#e9eeee"}}/>} />
            <SideBarItem route="/" title={"Technische Vraagstellingen"} icon={<DataObjectIcon sx={{color: "#e9eeee"}}/>} />
            <SideBarItem route="/validated-queries" title={"Gevalideerde Vragen"} icon={<QuestionAnswerIcon sx={{color: "#e9eeee"}}/>} />
          </List>
        </Box>
      </Drawer>
  )
}
