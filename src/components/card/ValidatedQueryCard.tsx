import CardHeader from "@suid/material/CardHeader";
import Divider from "@suid/material/Divider";
import CardContent from "@suid/material/CardContent";
import Typography from "@suid/material/Typography";
import CardActions from "@suid/material/CardActions";
import Button from "@suid/material/Button";
import DeleteIcon from "@suid/icons-material/Delete";
import PrimaryCard from "~/components/card/PrimaryCard";
import {useValidatedQueries} from "~/contexts/ValidatedQueryContext";
import {ValidatedQuery} from "~/components/model/ValidatedQuery";
import CertificateIcon from "~/icon/CertificateIcon";
import {For} from "solid-js";
import {notificationService} from "@hope-ui/solid";
import KeyIcon from "~/icon/KeyIcon";
import VerifiableCredentialCard from "./VerifiableCredentialCard";


interface Props {
  validatedQuery: ValidatedQuery;
}

const deleteQueryOnServer = async (validatedQuery: ValidatedQuery) => {
  if(window.confirm(`Weet je zeker dat je de query "${validatedQuery.name}" wilt verwijderen?`)) {
    const response = await fetch(`${import.meta.env.VITE_BASE_URL_API}/v1/validated-query`, {
      method: "delete",
      body: JSON.stringify(validatedQuery),
      headers: {
        'Content-Type': 'application/json'
      },
    });

    if(!response.ok) {
      const jsonResponse = await response.json();
      notificationService.show({
        status: "danger",
        title: "Er is iets fout gegaan",
        description: "Het verwijderen is helaas mislukt. De server meldt het volgende: " + jsonResponse.message
      });
    }

    return response.ok;
  }
  return false;
}

export default ({validatedQuery}: Props) => {

  // @ts-ignore
  const [queries, {removeValidatedQuery}] = useValidatedQueries();

  return (
      <PrimaryCard elevation={4}>
        <CardHeader
            avatar={<CertificateIcon />}
            title={<Typography variant="h5">{validatedQuery.name}</Typography>}
        />
        <Divider/>
        <CardContent>
          <Typography variant="h5" gutterBottom>Issuer</Typography>
          {validatedQuery.issuerName} ({validatedQuery.issuerCity})
          <Typography variant="h5" gutterBottom>Subject</Typography>
          {validatedQuery.subjectName} ({validatedQuery.subjectCity})
          <For
              each={validatedQuery.queries}
          >{(query, index) =>
            <>
              <Typography variant="h5" gutterBottom mt={2}>Query {index() + 1}</Typography>
              {query.name}
            </>
          }</For>
          <Typography variant="h5" gutterBottom>Credentials</Typography>
          <For each={validatedQuery.verifiableCredentials}>{(credential) =>
              <VerifiableCredentialCard verifiableCredential={credential} />
          }</For>
        </CardContent>
        <Divider/>
        <CardActions sx={{display: "flex", flexDirection: "column", alignItems: "flex-end"}}>
          <Button
              variant="outlined"
              color="error"
              startIcon={<DeleteIcon/>}
              onClick={async () => {
                if(await deleteQueryOnServer(validatedQuery)) {
                  removeValidatedQuery(validatedQuery); //remove from the store
                }
              }}
          >
            Delete
          </Button>
        </CardActions>
      </PrimaryCard>
  )
}
