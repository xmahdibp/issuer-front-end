import CardHeader from "@suid/material/CardHeader";
import KeyIcon from "~/icon/KeyIcon";
import PrimaryCard from "~/components/card/PrimaryCard";
import {createSignal} from "solid-js";
import {Collapse} from "@hope-ui/solid";
import ExpandMoreIcon from '@suid/icons-material/ExpandMore';
import IconButton from "@suid/material/IconButton";
import CardContent from "@suid/material/CardContent";

interface Props {
  verifiableCredential: any;
}

export default ({verifiableCredential}: Props) => {

  const [expanded, setExpanded] = createSignal(false);

  return (

      <PrimaryCard elevation={6}>
        <CardHeader
            title={JSON.parse(verifiableCredential).credentialSubject.validatedQuery.name}
            avatar={<KeyIcon/>}
            action={<IconButton
                onClick={() => setExpanded(!expanded())}
                sx={{
                  transform: !expanded() ? 'rotate(0deg)' : 'rotate(180deg)',
                  float: 'right',
                  '&:focus': {
                    outline: 'none',
                    boxShadow: 'none'
                  }
            }}
            >
              <ExpandMoreIcon/>
            </IconButton>}
        >

        </CardHeader>
        <Collapse expanded={expanded()}>
          <CardContent sx={{overflow: 'auto'}}>
              <pre>{verifiableCredential}</pre>
          </CardContent>
        </Collapse>
      </PrimaryCard>
)}
