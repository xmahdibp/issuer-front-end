import Card from "@suid/material/Card";
import styled from '@suid/system/styled';

// @ts-ignore
const PrimaryCard = styled(Card)(({ theme }) => ({
  backgroundColor: 'white',
  whiteSpace: "pre-wrap",
  color: '#000',
  marginBottom: '10px',
  position: 'relative',
  '&>div': {
    position: 'relative',
    zIndex: 5
  },
  '&:after': {
    content: '""',
    position: 'absolute',
    width: 210,
    height: 210,
    background: 'rgba(0, 0, 0, 0.4)',
    borderRadius: '50%',
    zIndex: 1,
    top: -85,
    right: -95,
    [theme.breakpoints.down('sm')]: {
      top: -105,
      right: -140
    }
  },
  '&:before': {
    content: '""',
    position: 'absolute',
    zIndex: 1,
    width: 210,
    height: 210,
    background: 'rgba(0, 0, 0, 0.4)',
    borderRadius: '50%',
    top: -125,
    right: -15,
    opacity: 0.5,
    [theme.breakpoints.down('sm')]: {
      top: -155,
      right: -70
    }
  },
  '.MuiCardHeader-title': {color: '#000'},
  '.MuiCardHeader-subheader': {color: '#bbb'},
}));

export default PrimaryCard;
