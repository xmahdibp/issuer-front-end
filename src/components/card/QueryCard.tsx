import CardHeader from "@suid/material/CardHeader";
import Divider from "@suid/material/Divider";
import CardContent from "@suid/material/CardContent";
import Typography from "@suid/material/Typography";
import CardActions from "@suid/material/CardActions";
import Button from "@suid/material/Button";
import DeleteIcon from "@suid/icons-material/Delete";
import PrimaryCard from "~/components/card/PrimaryCard";
import {Query} from "~/components/model/Query";
import {useQueries} from "~/contexts/QueryContext";
import ReportSearch from "~/icon/ReportSearch";
import {notificationService} from "@hope-ui/solid";

interface Props {
  query: Query;
}

const deleteQueryOnServer = async (query: Query) => {
  if(window.confirm(`Weet je zeker dat je de query "${query.name}" wilt verwijderen?`)) {
    const response = await fetch(`${import.meta.env.VITE_BASE_URL_API}/v1/query`, {
      method: "delete",
      body: JSON.stringify(query),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    if(!response.ok) {
      const jsonResponse = await response.json();
      notificationService.show({
        status: "danger",
        title: "Er is iets fout gegaan",
        description: "Het verwijderen is helaas mislukt. De server meldt het volgende: " + jsonResponse.message
      });
    }

    return response.ok;
  }
  return false;
}

export default ({query}: Props) => {

  // @ts-ignore
  const [queries, {removeQuery}] = useQueries();

  return (
      <PrimaryCard elevation={4}>
        <CardHeader
            avatar={<ReportSearch />}
            title={<Typography variant="h5">{query.name}</Typography>}
        />
        <Divider/>
        <CardContent>
          <Typography variant="h5" gutterBottom>Beschrijving</Typography>
          {query.description}
          <Typography variant="h5" gutterBottom mt={2}>Ontologie</Typography>
          {query.ontology}
          <Typography variant="h5" gutterBottom mt={2}>Query</Typography>
          <pre>{query.query}</pre>
          <Typography variant="h5" gutterBottom mt={2}>Uitwisselprofiel</Typography>
          <a href={query.profile.url} target="_blank">{query.profile.url} - ({query.profile.name})</a>
        </CardContent>
        <Divider/>
        <CardActions sx={{display: "flex", flexDirection: "column", alignItems: "flex-end"}}>
          <Button
              variant="outlined"
              color="error"
              startIcon={<DeleteIcon/>}
              onClick={async () => {
                if(await deleteQueryOnServer(query)) {
                  removeQuery(query); //remove from the store
                }
              }}
          >
            Delete
          </Button>
        </CardActions>
      </PrimaryCard>
  )
}
