# Issuer Front End
This project mimics a front-end for the data-issuer. It shows the available organizations from the NUTS node and visualizes the creation of `VerifiableCredentials` process step-by-step.

## Building

Solid apps are built with _adapters_, which optimise your project for deployment to different environments.

By default, `npm run build` will generate a Node app that you can run with `node build`. To use a different adapter, add it to the `devDependencies` in `package.json` and specify in your `vite.config.js`.
