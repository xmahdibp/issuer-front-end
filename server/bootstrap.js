import express from "express";
import fallback from "express-history-api-fallback";

const app = express();
const { PORT = 5000 } = process.env;
const buildDir = "dist";

app.use(express.static(buildDir));
app.use(fallback("index.html", { root: buildDir }));

app.listen(PORT, () => console.log(`running on http://localhost:${PORT}`));
